## 1：项目说明

这是一套基于`Nodejs` + `koa2` + `Typescript` + `Routing-controllers` 纯手工搭建的项目基本框架，可以直接用在`传统网站`+`管理后台`的项目开发中。

就算你不会`php`，不会`java`也能够让前端的你无缝的对接和使用，独立开发网站不再是梦...

本项目中已经集成了：

- 1：`koa-log4`实现系统日志（程序日志access，运行日志application），日志放在`logs`文件夹中，当然你也可以在`/src/config`中指定日志输入路径。

- 2：基于`ApiDoc`实现接口帮助文档，可在方法上使用注释的方式生成接口文档，运行命令：`npm run doc` 生成文档。

- 3：接口测试，主要在`rest/`文件夹中，使用`*.http`进行接口调试。

- 4：实现`k-init cli`可以直接通过命令创建各种文件。

- 5：`Pm2` 实现负载均衡，上线部署使用命令：`npm run online`。

- 6：`七牛云` 实现资源文件的上传和存储，案例请看：[前端demo.vue](https://gitee.com/bmycode/VRofficialWebsite/blob/master/src/resources/view/admin/src/view/template/demo.vue)，后端：[AdminController.ts](https://gitee.com/bmycode/VRofficialWebsite/blob/master/src/controller/admin/AdminController.ts)。

- 7：~~集成`支付宝支付`功能，可以直接配置然后使用（PS：支付涉及很多核心参数配置，所以该功能被删除，如需要请QQ私聊）~~。

- 8：基于`Java`开发的思想，设计`service层`，`dao层`，`entity层`，`model层`等，当然在`Node.js`中使用略有差异，不影响。

- 9：基于还未正式发布的`Vue3` + `Element` 实现管理后台页面，管理后台已实现登录权限控制等。

- 10：`PC`端的前端主要使用`require.js`实现`js`模块化，`less`+`jade`实现`Css`样式和`Html`。

- 11：数据库基于`Mysql`，数据库操作使用`Node ORM`：`typeorm`，如果不熟悉`Mysql`又不想去学习，那么可以自己集成第三方后端云数据库，减少数据库相关的学习和开发成本。

- 12：~~基于新的打包工具`parcel`对管理后台的`vue`页面进行打包，后面会重新改造为`Vue3`作为项目管理后台~~

- 13：基于`gulp`完成对`PC`前端`less`的编译和静态资源的复制等。



## 2：技术栈

---

- 1: TypeScript
- 2: Koa2.js(全家桶)
- 3: jade + less 
- 4: GraphQL
- 5: TypeOrm 
- 6: TypeDi 
- 7: class-validator
- 8: Gulp
- 9: Routing-controllers
- 10: Require.js
- 11: Vue.js(全家桶) & TypeScript
- 12: Node.js
- 13: Parcel
- 14：Nginx 负载均衡
- 15: Docker 
- 16: Mysql，主从读写分离
- 17: Bmob 在线后端云
- 18: 七牛视频云存储方案
- 19: 阿里云免费 https 证书
- 21: SpringBoot 微服务全家桶技术
- ... 此处省略一万字


## 3：k-init cli 使用帮助 

参考`PHP`的框架`Laravel`提供的命令`artisan`实现终端创建项目中各种文件的功能，解决新增页面需要手工创建一大堆文件的麻烦！！

命令：

```bash 
$ ./bin/kinit -h

Usage: kinit [options]

Options:
  -t, --file-type <type>  文件类型: [controller，dao，entity，model，service，impl，jade，less，js]
  -n, --file-name <name>  被创建的文件名称
  -p, --file-path <path>  为 -t 参数指定 controller 文件夹名
  -i, --is-create <is>    是否为 -t 参数自动创建相关文件类型 (default: false)
  -v, --version           显示当前的版本
  -h, --help              display help for command
```

### 例子 1：

比如现在需要在`src/controller/api`下单独创建接口，那么可以使用命令：

```bash
$ ./bin/kinit -t controller -p api -n Test
```

参数说明：

- `-t controller`：表示创建的文件类型为`controller`
- `-p api`：表示需要在`src/controller/`文件夹的`api/`文件夹下创建文件，因为`controller`里面有三个文件夹，所以需要指定文件夹名称
- `-n Test`：表示创建的文件名称是`Test`

### 例子 2：

现在我需要新增一个`PC`官网前台页面，比如**详情页(Info)**，那么按照先前传统的过程我们需要手工去创建：`controller`，`dao`，`entity`，`model`，`service`，`impl`，`jade`，`less`，`js`等一堆文件！真的很烦，而且还要在这么多的文件中写入测试代码，更烦！！

这种需求很常见，但是现在不必忍受，因为你可以使用命令来自动化创建所有文件，文件中包含实例代码：

```bash
$ ./bin/kinit -t controller -p home -n Info -i true
```

参数说明(其他参数和上面一样)：

- `-i true`：表示是否需要为`Info`自动创建其他更多的各种文件，需要：`true`，不需要：`false` 或者 不写参数`-i`即可。

### 例子 3：

创建其他类型的文件，命令都一样。例如：

```bash
$ ./bin/kinit -t entity -n Test   # 创建 TestEntity      数据库实体
$ ./bin/kinit -t model -n Test    # 创建 TestModel       这里主要做数据校验的功能
$ ./bin/kinit -t dao -n Test      # 创建 TestDao         数据库操作层
$ ./bin/kinit -t service -n Test  # 创建 TestService     Service层
$ ./bin/kinit -t impl -n Test     # 创建 TestServiceImpl Service实现层 
$ ./bin/kinit -t jade -n Test     # 创建 Test.jade       静态页面View层
$ ./bin/kinit -t js -n Test       # 创建 Test.js         静态页面的js逻辑
$ ./bin/kinit -t less -n Test     # 创建 Test.less       静态页面的css样式
```

## 4：插件 routing-controllers 的坑

- Bug: 使用注解 `@Render` 渲染模板后，第二次访问其他页面但是展示效果还是上次的页面，页面不进行渲染

- 路径：`node_modules/routing-controllers/driver/koa/KoaDriver.js`
- 行数：`250`行

解决: 删除下面代码：

```js
// 保留，不要修改
var renderOptions_1 = result && result instanceof Object ? result : {};

//  开始 - 下面的都删除
this.koa.use(function (ctx, next) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, ctx.render(action.renderedTemplate, renderOptions_1)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
});
//  结束 - 上面的都删除
````

删除后，使用使用下面代码替换：

 ```js
 return options.response.render(action.renderedTemplate, renderOptions_1);
 ```

最后改好的的样子如下：

```js
var renderOptions_1 = result && result instanceof Object ? result : {};
return options.response.render(action.renderedTemplate, renderOptions_1);
```

- Github 问题反馈地址：[https://github.com/../434](https://github.com/typestack/routing-controllers/pull/434)

- Github 修复分支记录：[https://github.com/types..](https://github.com/typestack/routing-controllers/pull/434/files/001b45a416e3f1dbde33e7e91fadb09111d4061d)
