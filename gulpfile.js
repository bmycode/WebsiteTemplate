var gulp = require('gulp')
var less = require('gulp-less')

/**
 * 编译 resources 文件夹中的 less 文件为 css 重新存放到 css/ 文件夹中
 */
gulp.task('less', function () {
    return gulp.src('src/resources/static/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('src/resources/static/css/'))
});

/**
 * 复制 resources 到 dist 文件，但是 排除 less 文件夹
 * 同时也排除管理后台 admin/ 文件夹，因为管理后台是 vue + typesc 写的独立项目
 * 需要在终端单独启动 admin/ 管理后台
 */
gulp.task('copy',  function() {
    return gulp.src(['src/resources/**','!src/**/less/**','!src/**/less','!src/resources/**/admin/**','!src/resources/**/admin'], { base: 'src/' })
      .pipe(gulp.dest('dist/'))
});

/**
 * 1：监听 resources 文件夹中修改操作，然后执行 copy 任务
*  2：监听 resources 文件夹中less的修改操作，然后执行 less 任务
 */
gulp.task('auto', function () {
    gulp.watch('src/resources/**/*', gulp.parallel('copy'))
    gulp.watch('src/resources/static/less/**/*.less', gulp.parallel('less'))
});



gulp.task('default', gulp.parallel('less','copy','auto'))