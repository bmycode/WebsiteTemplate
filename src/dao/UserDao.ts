import { Service , Inject } from "typedi";
import { UserEntity } from "../entity/UserEntity";
import { getRepository } from "typeorm";

@Service()
export class UserDao {
	
	/**
	 * 查询用户具体信息
	 * @param id
	 */
	public async getOneUserDao(id: string): Promise<any> {
		return await getRepository(UserEntity)
			.findOne({ where: { user_id: id} });
	}
	
	public async AllUser(): Promise<any> {
		return await getRepository(UserEntity)
		.find();
	}
	
}