import { KoaMiddlewareInterface } from "routing-controllers";

/**
 * 判断用户是否登录，如果登录则继续访问，没有去登录
 */
export class CheckLoginMiddleware implements KoaMiddlewareInterface {
  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    if (!context.session || !context.session.user) {
      context.redirect(`/reg?from=${context.request.url}`);
    }else {
      return next();
    }
  }
}