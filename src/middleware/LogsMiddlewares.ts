import { KoaMiddlewareInterface, Action } from "routing-controllers";
import { Service,Inject } from "typedi";
import { Logs } from "../utils/logs";
import moment from "moment";

/**
 * controller 拦截器
 * 拦截 controller 被访问前，进行访问的日志记录
 */
@Service()
export class LogsMiddlewares implements KoaMiddlewareInterface {
  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    if (context.request.method == "GET") {
      Logs.ApplicationLogger().warn(`${moment().format('YYYY-MM-DD HH:mm:ss')} ${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.querystring)} ${JSON.stringify(context.request.header)} \n`);
    } else {
      Logs.ApplicationLogger().warn(`${moment().format('YYYY-MM-DD HH:mm:ss')} ${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.body)} ${JSON.stringify(context.request.header)} \n`);
    }
    return next()
  }
}