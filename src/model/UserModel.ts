import { MinLength,IsInt, MaxLength,Allow,Validate } from 'class-validator';

export class UserModel {
  @MinLength(1,{
    message: 'id参数必须有'
  })
  id: string;
}