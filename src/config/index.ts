// 开发环境配置
import { dev } from "./dev";
// 上线环境配置
import { prod } from "./prod";

export const config = process.env.NODE_ENV == "dev" ? dev : prod;