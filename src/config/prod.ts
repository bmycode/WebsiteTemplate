import path from "path";

/**
 * 上线阶段的环境配置
 */
export const prod = {
  port: 9090,
  host: "127.0.0.1",
  apiurl: '/bmy/api',
  uploadPath: '../resources/static/upload/',
  initPlugins: {
    SessionKey: '4WM7mSy5S5kkxcE1I3',
    staticPath: '../resources/static',
    viewsPath: '../resources/view'
  },
  banner: {
    welcome: () => {
      return `
        👏  VR 产品介绍网站 👏

        👉  1：PC官网: http://${prod.host}:${prod.port}/
        👉  2：Admin后台: http://${prod.host}:3030/#/course/all
        `
    }
  },
  logConfig: {
    appenders: {
      access: {
        type: 'console',
        pattern: '-yyyy-MM-dd.log', //通过日期来生成文件
        alwaysIncludePattern: true, //文件名始终以日期区分
        encoding:"utf-8",
        filename: path.join(__dirname, '../../logs/access.log') //生成文件路径和文件名
      },
      //系统日志
      application: {
        type: 'console',
        pattern: '-yyyy-MM-dd.log', //通过日期来生成文件
        alwaysIncludePattern: true, //文件名始终以日期区分
        encoding:"utf-8",
        filename: path.join(__dirname, '../../logs/application.log') //生成文件路径和文件名
      },
      out: {
        type: 'console'
      }
    },
    categories: {
      default: { appenders: [ 'out' ], level: 'info' },
      access: { appenders: [ 'access' ], level: 'info' },
      application: { appenders: [ 'application' ], level: 'WARN'}
    }
  },
  qiniu: {
    accessKey: '0MGmT_rkMiaOeXY09B4EhBnXuDcIYyKlGumQ-zUt',
    secretKey: '59dfmEi9Q50rMlz9sxTBCvYDhM7biERAjflBXALb',
    HLSconfig: {
      VideoBaseUrl: 'http://video.geekhelp.cn/',
      FromBucket: 'guaikevideo',
      ToBucket: 'm3u8video',
      VideoHlsParams: 'avthumb/m3u8/segtime/10/ab/128k/ar/44100/acodec/libfaac/r/30/vb/640k/vcodec/libx264/stripmeta/0/noDomain/1|saveas/'
    }
  },
  title: ' | 怪客课堂',
  typeorm: {
    type: 'mysql',
    host: "localhost",
    charset: "utf8_general_ci",
    port: 3306,
    username: "root",
    password: "lb714500",
    database: "shop",
    logging: "error",
    logger: "file",
    synchronize: false,
    maxQueryExecutionTime: 1000,
    cache: {
      duration: 30000
    },
    entities: [
      `${path.join(__dirname,'../entity/*{.js,.ts}')}`
    ]
  }
};