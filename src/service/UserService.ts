export interface UserService {
	getOneUserService(id: string) : Promise<any>;
	getAllUserService() : Promise<any>;
}