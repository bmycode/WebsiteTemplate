import { UserService } from "../UserService";
import { Service, Inject } from "typedi";
import { UserDao } from "../../dao/UserDao";

@Service()
export class UserServiceImpl implements UserService{
	
	@Inject()
	private userDao: UserDao;
	
	public async getOneUserService(id: string): Promise<any> {
		return await this.userDao.getOneUserDao(id);
	}
	
	public async getAllUserService(): Promise<any> {
		return await this.userDao.AllUser();
	}
}