import "reflect-metadata";
import { useKoaServer,useContainer } from "routing-controllers";
import { Container } from "typedi";
import { config } from "../config";
import { Logs } from "../utils/logs";
import { initPlugins } from "./init";
import { logger } from "../utils/logger";

import path from "path";

export class App extends initPlugins{
  private Controller: Function[];
  private app: any;

  constructor () {
    super();
    // typedi 注入到 routing-controllers
    useContainer(Container);
    this.createKoa();
  }

  /**
   * 创建 koa 服务
   */
  createKoa() {
    this.app = useKoaServer(this.Koa2, {
      cors: true,
      validation: true,
      classTransformer: true,
      controllers: [`${path.join(__dirname, '../controller/**/*{.js,.ts}')}`]
    });
    this.run();
  }

  // 启动程序
  private run (): any {
    try {
      this.app.listen(config.port, () => logger.logSymbolsSuccess(config.banner.welcome()));
    } catch (e) {
      logger.logSymbolsError(`❌ 启动出错：${e.message}`);
      Logs.ApplicationLogger().error(e);
    }
    this.errorCatch();
    // this.Page_404();
  }

  // 错误捕捉Unsupported type
  private errorCatch (): any {
    this.app.on('error', err => {
      logger.logSymbolsError(`❌ 主程序捕捉到错误：${err.message}`);
      Logs.ApplicationLogger().error(err);
    });
  }
  
  /**
   * 捕捉到404的时候跳转到error错误页面
   * @constructor
   */
  private Page_404 () {
    this.app.use(async ctx => {
      if (ctx.response.status == 404) {
        ctx.redirect('/error');
      }
    })
  }

}