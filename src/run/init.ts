import KoaBodyParser from "koa-bodyparser";
import serve from "koa-static";
import views from "koa-views";
import session from "koa-session";
import path from "path";
import koa from "koa";
import { logger } from "../utils/logger";
import { Logs } from "../utils/logs";
import { config } from "../config";
import { createConnection } from "typeorm";

/**
 * 把第三方插件 和 中间件 的配置从主启动类中剥离处理
 * 便于项目后期配置更多的第三方插件&中间件
 */
export class initPlugins {
	// 所有需要配置参数的中间件集合
	public PluginsList: Array<any>;
	public Koa2: any = new koa();
	constructor () {
		this.combinationPlugins();
	}
	
	/**
	 * 组装koa2的中间件为Array
	 */
	private async combinationPlugins () {
		
		
		// 设置签名的 Cookie 密钥。
		this.Koa2.keys = [ config.initPlugins.SessionKey ];
		this.PluginsList = [
			// 记录系统日志，访问级别的，记录用户的所有请求，作为koa的中间件
			Logs.AccessLogger(),
			// 处理post请求
			KoaBodyParser(),
			// 静态资源配置
			serve(path.join(__dirname, config.initPlugins.staticPath ), { maxage: 0 }),
			// session 配置
			session({
				key: 'koa:sess',
				maxAge: 86400000,
				overwrite: true,
				httpOnly: true,
				signed: true,
				rolling: false,
				renew: false,
			},this.Koa2),
			// 模板引擎配置
			views(path.join(__dirname, config.initPlugins.viewsPath ), {
				extension: 'jade',
				options: { ext: 'jade' }
			})
		];
		
		try {
			this.PluginsList.forEach((v) => this.Koa2.use(v));
			// @ts-ignore
			await createConnection(config.typeorm);
		} catch (e) {
			// 终端输出
			logger.logSymbolsError(`配置插件出错：${e.message}`);
			Logs.ApplicationLogger().error(e);
		}
	}
}