import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity("user")
export class UserEntity {
	
	@PrimaryGeneratedColumn()
	user_id: number;
	
	@Column()
	user_name: string;
	
	@Column()
	user_pwd: string;
	
	@Column()
	user_time: string;
	
	@Column()
	user_money: string;
	
	@Column()
	user_pic: string;
	
	@Column()
	user_address: string;
	
	@Column()
	user_phone: string;
	
}