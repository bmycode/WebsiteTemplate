import log4 from "koa-log4";
import { config } from "../config";
log4.configure(config.logConfig);

export class Logs {
	/**
	 * 访问级别的日志
	 */
	public static AccessLogger() {
		return log4.koaLogger(log4.getLogger('access'), { level: 'auto' });
	}
	
	/**
	 * 应用级别的日志
	 * @constructor
	 */
	public static ApplicationLogger() {
		return log4.getLogger('application')
	}
	
	public static HttpLogger() {
		return log4.getLogger('request')
	}
	
}