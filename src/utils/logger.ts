import logSymbols from "log-symbols";
import chalk from "chalk";
export class logger {
    public static logSymbolsSuccess(message:string): void {
        console.log(chalk.green.bold(logSymbols.success, message));
    }
    public static logSymbolsInfo(message:string): void {
        console.log(logSymbols.info, message);
    }
    public static logSymbolsWarning(message:string): void {
        console.log(logSymbols.warning, message);
    }
    public static logSymbolsError(message:string): void {
        console.log(chalk.bold.red(logSymbols.error, message));
    }
}