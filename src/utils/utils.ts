import { config } from "../config/index";
import fs from "fs";
import path from "path";

export class utils {

    /**
     * 统一设置网站浏览器的 title 标题内容
     * @param title 
     */
    public static titleName(title:string):string {
        return `${title}${config.title}`
    }
    
    /**
     * 将get地址栏参数转换为对象
     * @param url
     */
    public static parseQueryString (url) {
        var reg_url = /^[^\?]+\?([\w\W]+)$/,
          reg_para = /([^&=]+)=([\w\W]*?)(&|$|#)/g,
          arr_url = reg_url.exec(url),
          ret = {};
        if (arr_url && arr_url[1]) {
            var str_para = arr_url[1], result;
            while ((result = reg_para.exec(str_para)) != null) {
                ret[result[1]] = result[2];
            }
        }
        return ret;
    }
}