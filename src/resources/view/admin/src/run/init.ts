import '../utils/class-component-hooks'
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

// ElementUI
import 'element-ui/lib/theme-chalk/index.css';

// Vuex，config 全站配置
import Stores from '../store/index'
import { config } from "../config"

/**
 * 初始化Vue需要的插件和构造Vue启动参数
 */
export class init {

	// 存放 Vue 插件 的数组
	private initVuePlugsArray: Array<any> = config.VuePlugs;
	// 存放 非Vue插件 的数组
	private initOtherPlugsArray: Array<Object> = config.NotVuePlugs;
	// 存放配置好的 Vue-router
	protected router: any;
	// 存放配置好的 Vuex
	protected VuexConfig: any;

	constructor () {
		Vue.config.productionTip = false;
		this.initPlugs()
	}

	/**
	 * 1 统一初始化 Vue 插件
	 * 2 统一挂载非 Vue 插件
	 */
	private initPlugs ():void {
		// @ts-ignore use 挂载Vue需要的插件
		this.initVuePlugsArray.forEach(v => Vue.use(v));
		// @ts-ignore 初始化非Vue插件
		this.initOtherPlugsArray.forEach(v => Vue.prototype[v['n']] = new (v['f'])());

		this.InitVueRouter();
		this.InitVuex();
	}

	/**
	 * 1：初始化Vue-router
	 * 2：并配置全局拦截器
	 * @constructor
	 */
	private InitVueRouter ():void {
		this.router = new VueRouter(config.RouterConfigUrl);
		this.router.beforeEach((to:any, from:any, next:any) => {
			document.title = to.meta.title;
			if(to.meta.ifLogin) {
				console.log(localStorage.getItem("status"))
				if(localStorage.getItem("status") != null){
					next()
				} else {
					next({
						path:"/login",
						query: {
							redirect: to.fullPath
						}
					})
				}
			}else {
				next()
			}
		});
	}

	/**
	 * 初始化Vuex
	 * @constructor
	 */
	private InitVuex ():void {
		this.VuexConfig = new Vuex.Store(Stores)
	}
}