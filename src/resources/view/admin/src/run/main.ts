import Vue from 'vue'
import App from '../App.vue';
import { init } from "./init";
import { config } from "../config/index"

export class Main extends init {
	constructor () {
		super();
		this.initApp();
	}
	/**
	 * 启动Vue，传入配置参数 initVue
	 * 挂载到 config.mountElement 上
	 */
	public initApp():void  {
		const router = this.router, store = this.VuexConfig;
		new Vue({
			router,
			store,
			render: (h):any => h(App)
		}).$mount(config.mountElement);
	}
}