import index from "./modules/index"
import getters from "./getters"

// 统一导出各模块
export default {
	modules: { ...index },
	getters
}