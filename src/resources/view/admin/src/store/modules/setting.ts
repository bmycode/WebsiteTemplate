export default {
	state: {
		isClosedHeader: true,
		isClosedFooter: true,
		isClosedAside: true
	},
	mutations: {
		SET_BAR: (state:any, status: boolean) => {
			state.isClosedHeader = status;
			state.isClosedFooter = status;
			state.isClosedAside = status;
		}
	}
}