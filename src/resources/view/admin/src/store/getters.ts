// getter 读取方法
export default {
	isClosedFooter: (state:any) => state.Setting.isClosedFooter,
	isClosedHeader: (state:any) => state.Setting.isClosedHeader,
	isClosedAside: (state:any) => state.Setting.isClosedAside,
	getClassList: (state:any) => state.Main.classData
}