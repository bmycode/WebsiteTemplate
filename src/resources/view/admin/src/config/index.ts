import LoginComponents from '../view/template/login.vue';
import CourseComponents from '../view/template/course/index.vue';
import AllComponents from '../view/template/course/all.vue';
import NewComponents from '../view/template/course/new.vue';
import NewVideoComponents from '../view/template/course/newVideo.vue';
import AllClassComponents from '../view/template/classify/all.vue';
import DemoComponents from '../view/template/demo.vue';

import { Axios } from "../api/axios";
import { Utils } from "../utils/index";

import Vuex from 'vuex';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';


export class config {
	public static mountElement: string = "#app";
	// 需要被挂载的非Vue插件
	public static NotVuePlugs: Array<Object> = [
		{ n: '$http', f: Axios },
		{ n: '$utils', f: Utils }
	];
	public static VuePlugs: Array<any> = [ ElementUI, VueRouter, Vuex ]
	// 路由配置
	public static RouterConfigUrl: Object = {
		mode: 'hash',
		routes: [
			{
				path: '/demo',
				name: 'demo',
				component: DemoComponents,
				meta: {
					ifLogin: false,
					title: "测试文件"
				}
			},
			{
				path: '/',
				redirect: {
					name: 'login'
				}
			},
			{
				path: '/login',
				name: 'login',
				component: LoginComponents,
				meta: {
					ifLogin: false,
					title: "管理员登录"
				}
			},
			{
				path: '/course',
				component: CourseComponents,
				children: [
					{
						name: 'all',
						path: 'all',
						component: AllComponents,
						meta: {
							ifLogin: true,
							title: "所有课程"
						}
					},
					{
						name: 'new',
						path: 'new',
						component: NewComponents,
						meta: {
							ifLogin: true,
							title: "发布课程"
						}
					},
					{
						name: 'video',
						path: 'video/:id',
						component: NewVideoComponents,
						meta: {
							ifLogin: true,
							title: "发布课程视频"
						}
					}
				]
			},
			{
				path: '/class',
				name: 'allclass',
				component: AllClassComponents,
				meta: {
					ifLogin: true,
					title: "所有课程分类"
				}
			}
		]
	}
}