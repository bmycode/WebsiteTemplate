import WatchJS from 'melanke-watchjs';

export class Utils {
	public watch:any;
	public NewTargetArray:Array<any> = [];
	
	constructor() {
		this.watch = WatchJS.watch;
	}
	
	/**
	 * 返回上一页
	 */
	goBack() {
		history.back()
	}
	
	/**
	 * 调用 封装的工具类对 对象进行 set 拦截并返回
	 * 被修改数据的对象所在的下标。
	 * @param OldTarget 需要观察的数据
	 * @param cb 回调函数
	 * @constructor
	 */
	public WatchData (OldTarget: Array<any>, cb: Function): void {
		OldTarget.forEach( (val,index) => {
			this.watch(val, (prop, action, newvalue, oldvalue) => {
				cb(index)
			})
		});
	}
	
	/**
	 * 加密
	 * @param code
	 */
	public compile(code: any) {
		code = JSON.stringify(code);
		let c = String.fromCharCode(code.charCodeAt(0)+code.length);
		for(let i = 1; i < code.length; i++){
			c += String.fromCharCode(code.charCodeAt(i)+code.charCodeAt(i-1));
		}
		return (escape(c));
	}
	
	/**
	 * 解密
	 * @param code
	 */
	public uncompile(code: any) {
		code = unescape(code);
		let c = String.fromCharCode(code.charCodeAt(0)-code.length);
		for(let i = 1; i < code.length; i++){
			c +=String.fromCharCode(code.charCodeAt(i)-c.charCodeAt(i-1));
		}
		return c;
	}
}