define(['jquery','config','zui'],function ($,config) {

  function utils() {
    this.init()
  }

  utils.prototype = {
    init () {
    },

    // banner高度等于显示器高度
    setBannerHeight () {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        $("body").css({  "width": $(document).width() });
      } else {
        $(".header").css({  "height": $(window).height() });
        $("body").css({  "width": "100%" });
      }
    },

    getUserId () {
      if(localStorage.getItem("objectId") != null ) {
        return localStorage.getItem("objectId")
      } else {
        location.href = `/reg?from=${location.pathname}`
      }
    },

    /**
     * 获取地址栏参数
     * @param name 需要获取的名称
     * @return {string|null}
     * @constructor
     */
    GetQueryString(name) {
      var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
      var r = window.location.search.substr(1).match(reg);
      if(r!=null)return  unescape(r[2]); return null;
    },

    InfoCourseId () {
      return (location.pathname.split("/info/"))[1]
    },

    /**
     *
     * @param params object 参数
     * {
     *   url: '请求地址',
     *   data: '请求参数'
     * }
     * @param callback function 回调函数
     */
    get (params = {}, callback) {
      $.ajax({
        type: 'GET',
        url: `${config.baseURL}${params.url}`,
        dataType: 'json',
        data: params.data,
        success: function (res) {
          if(res.status = 200) {
            callback(res)
          } else {
            uikit.notification({
              message: `GET 接口报错: ${err.message}`,
              status: 'danger',
              pos: 'top-right',
              timeout: 5000
            });
          }
        },
        error: function (err) {
          uikit.notification({
            message: `GET 接口报错: ${err.message}`,
            status: 'danger',
            pos: 'top-right',
            timeout: 5000
          });
          throw new Error(`GET 接口报错 ${err.message}`)
        }
      })
    },

    /**
     *
     * @param params object 参数
     * {
     *   url: '请求地址',
     *   data: '请求参数'
     * }
     * @param callback function 回调函数
     */
    post (params = {}, callback) {
      $.ajax({
        type: 'POST',
        url: `${config.baseURL}${params.url}`,
        dataType: 'json',
        data: params.data,
        success: function (res) {
          if(res.status = 200) {
            callback(res)
          } else {
            uikit.notification({
              message: '网络不佳，请稍后重试',
              status: 'danger',
              pos: 'top-right',
              timeout: 5000
            });
          }
        },
        error: function (err) {
          uikit.notification({
            message: `POST 接口报错: ${err.message}`,
            status: 'danger',
            pos: 'top-right',
            timeout: 5000
          });
        }
      })
    }
  }

  return new utils();

})