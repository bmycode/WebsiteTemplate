import { JsonController, Get, UseBefore, QueryParams, Body, UseInterceptor, Post } from "routing-controllers";
import { config } from "../../config";
import { JsonResponseInterceptor } from "../../middleware/JsonResponseMiddlewares";
import { Service, Inject } from "typedi";
import { UserServiceImpl } from "../../service/impl/UserServiceImpl";
import { UserModel } from "../../model/UserModel";
import { LogsMiddlewares } from "../../middleware/LogsMiddlewares";

@Service()
@JsonController(`${config.apiurl}/user`)
@UseInterceptor(JsonResponseInterceptor)
export class UserControllerApi {

  @Inject()
  private userServiceImpl: UserServiceImpl;

  /**
   * @api {get} /user/oneUser  oneUser
   * @apiVersion 1.1.0
   * @apiDescription  获取一条用户记录
   * @apiName oneUser
   * @apiGroup user
   * @apiParam {string} id 用户id
   * @apiSuccessExample {json} 请求成功的返回:
   *  {
   *      "status" : 200,
   *      "result" : {
   *       }
   *  }
   * @apiErrorExample {json} 请求失败的返回
   * {
   *   "status":200,
   *   "result":{
   *    }
   * }
   * @apiSampleRequest http://www.geekhelp.cn/bmy/api/user/oneUser
   * @apiVersion 1.0.0
   */
  @Get("/oneUser")
  @UseBefore(LogsMiddlewares)
  public async oneUser (@QueryParams() query: UserModel): Promise<void> {
    return await this.userServiceImpl.getOneUserService(query.id);
  }

  /**
   * @api {get} /user/allUser  allUser
   * @apiVersion 1.1.0
   * @apiDescription  获取所有用户信息
   * @apiName allUser
   * @apiGroup user
   * @apiSuccessExample {json} 请求成功的返回:
   *  {
   *      "status" : 200,
   *      "result" : {
   *       }
   *  }
   * @apiErrorExample {json} 请求失败的返回
   * {
   *   "status":200,
   *   "result":{
   *    }
   * }
   * @apiSampleRequest http://www.geekhelp.cn/bmy/api/user/allUser
   * @apiVersion 1.0.0
   */
  @Get("/allUser")
  @UseBefore(LogsMiddlewares)
  public async allUser (): Promise<Object> {
    return await this.userServiceImpl.getAllUserService();
  }
  
}