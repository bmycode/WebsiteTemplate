import {Controller, Render, Get, UseBefore, Session} from "routing-controllers";
import { utils } from "../../utils/utils";

@Controller()
export class AboutControllerHome {
  @Get("/about")
  @Render("home/page/about")
  About(@Session() session) {
    return {
      title: utils.titleName("关于"),
      isShowAction: {
        header: true,
        footer: true
      },
      banner: {
        bannerTitle: '关于怪客',
        bannerUrl: '/img/banner_5.jpg',
      },
      login: session.user ? session.user : false
    }
  }
}