import {Controller, Render, Get, UseBefore, Session} from "routing-controllers";
import { utils } from "../../utils/utils";
import { Service, Inject } from "typedi";

@Service()
@Controller()
export class IndexControllerHome {
  
  @Get("/")
  @Render("home/page/index")
  public async Index(@Session() session,) {
    return {
      title: utils.titleName("首页"),
      isShowAction: {
        header: true,
        footer: true
      },
      login: session.user ? session.user : false
    }
  }
}