import {Controller, Render, Get, UseBefore, Session} from "routing-controllers";
import { utils } from "../../utils/utils";

@Controller()
export class ErrorControllerHome {
	@Get("/error")
	@Render("home/page/error")
	public Error() {
		return {
			title: utils.titleName("404..."),
			isShowAction: {
				header: false,
				footer: false
			}
		}
	}
}