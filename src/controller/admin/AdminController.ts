import { JsonController, Body, Post, UploadedFile,UseInterceptor, Req, Res, Get } from "routing-controllers";
import { JsonResponseInterceptor } from "../../middleware/JsonResponseMiddlewares";
import { Service, Inject } from "typedi";
import { config } from "../../config";
import { utils } from "../../utils/utils";
import { QiNiu } from "../../plugins/qiniu"

@Service()
@JsonController(`${config.apiurl}/admin`)
@UseInterceptor(JsonResponseInterceptor)

export class AdminController {

	@Inject()
	private utils: utils;
	@Inject()
	private qiniu:QiNiu;
	/**
	 * 获取上传token，服务于图片，视频的上传
	 * http://www.geekhelp.cn/bmy/api/admin/token
	 * @param params
	 * @constructor
	 */
	@Post("/token")
	public async Token (): Promise<Object> {
		return { qn_token: this.qiniu.GenerateToken() };
	}
	
}