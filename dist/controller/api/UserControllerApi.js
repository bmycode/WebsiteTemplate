"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const routing_controllers_1 = require("routing-controllers");
const config_1 = require("../../config");
const JsonResponseMiddlewares_1 = require("../../middleware/JsonResponseMiddlewares");
const typedi_1 = require("typedi");
const UserServiceImpl_1 = require("../../service/impl/UserServiceImpl");
const UserModel_1 = require("../../model/UserModel");
const LogsMiddlewares_1 = require("../../middleware/LogsMiddlewares");
let UserControllerApi = class UserControllerApi {
    /**
     * @api {get} /user/oneUser  oneUser
     * @apiVersion 1.1.0
     * @apiDescription  获取一条用户记录
     * @apiName oneUser
     * @apiGroup user
     * @apiParam {string} id 用户id
     * @apiSuccessExample {json} 请求成功的返回:
     *  {
     *      "status" : 200,
     *      "result" : {
     *       }
     *  }
     * @apiErrorExample {json} 请求失败的返回
     * {
     *   "status":200,
     *   "result":{
     *    }
     * }
     * @apiSampleRequest http://www.geekhelp.cn/bmy/api/user/oneUser
     * @apiVersion 1.0.0
     */
    oneUser(query) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userServiceImpl.getOneUserService(query.id);
        });
    }
    /**
     * @api {get} /user/allUser  allUser
     * @apiVersion 1.1.0
     * @apiDescription  获取所有用户信息
     * @apiName allUser
     * @apiGroup user
     * @apiSuccessExample {json} 请求成功的返回:
     *  {
     *      "status" : 200,
     *      "result" : {
     *       }
     *  }
     * @apiErrorExample {json} 请求失败的返回
     * {
     *   "status":200,
     *   "result":{
     *    }
     * }
     * @apiSampleRequest http://www.geekhelp.cn/bmy/api/user/allUser
     * @apiVersion 1.0.0
     */
    allUser() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userServiceImpl.getAllUserService();
        });
    }
};
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", UserServiceImpl_1.UserServiceImpl)
], UserControllerApi.prototype, "userServiceImpl", void 0);
__decorate([
    routing_controllers_1.Get("/oneUser"),
    routing_controllers_1.UseBefore(LogsMiddlewares_1.LogsMiddlewares),
    __param(0, routing_controllers_1.QueryParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [UserModel_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserControllerApi.prototype, "oneUser", null);
__decorate([
    routing_controllers_1.Get("/allUser"),
    routing_controllers_1.UseBefore(LogsMiddlewares_1.LogsMiddlewares),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserControllerApi.prototype, "allUser", null);
UserControllerApi = __decorate([
    typedi_1.Service(),
    routing_controllers_1.JsonController(`${config_1.config.apiurl}/user`),
    routing_controllers_1.UseInterceptor(JsonResponseMiddlewares_1.JsonResponseInterceptor)
], UserControllerApi);
exports.UserControllerApi = UserControllerApi;
