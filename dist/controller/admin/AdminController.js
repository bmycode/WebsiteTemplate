"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const routing_controllers_1 = require("routing-controllers");
const JsonResponseMiddlewares_1 = require("../../middleware/JsonResponseMiddlewares");
const typedi_1 = require("typedi");
const config_1 = require("../../config");
const utils_1 = require("../../utils/utils");
const qiniu_1 = require("../../plugins/qiniu");
let AdminController = class AdminController {
    /**
     * 获取上传token，服务于图片，视频的上传
     * http://www.geekhelp.cn/bmy/api/admin/token
     * @param params
     * @constructor
     */
    Token() {
        return __awaiter(this, void 0, void 0, function* () {
            return { qn_token: this.qiniu.GenerateToken() };
        });
    }
};
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", utils_1.utils)
], AdminController.prototype, "utils", void 0);
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", qiniu_1.QiNiu)
], AdminController.prototype, "qiniu", void 0);
__decorate([
    routing_controllers_1.Post("/token"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "Token", null);
AdminController = __decorate([
    typedi_1.Service(),
    routing_controllers_1.JsonController(`${config_1.config.apiurl}/admin`),
    routing_controllers_1.UseInterceptor(JsonResponseMiddlewares_1.JsonResponseInterceptor)
], AdminController);
exports.AdminController = AdminController;
