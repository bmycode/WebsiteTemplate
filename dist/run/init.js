"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa_static_1 = __importDefault(require("koa-static"));
const koa_views_1 = __importDefault(require("koa-views"));
const koa_session_1 = __importDefault(require("koa-session"));
const path_1 = __importDefault(require("path"));
const koa_1 = __importDefault(require("koa"));
const logger_1 = require("../utils/logger");
const logs_1 = require("../utils/logs");
const config_1 = require("../config");
const typeorm_1 = require("typeorm");
/**
 * 把第三方插件 和 中间件 的配置从主启动类中剥离处理
 * 便于项目后期配置更多的第三方插件&中间件
 */
class initPlugins {
    constructor() {
        this.Koa2 = new koa_1.default();
        this.combinationPlugins();
    }
    /**
     * 组装koa2的中间件为Array
     */
    combinationPlugins() {
        return __awaiter(this, void 0, void 0, function* () {
            // 设置签名的 Cookie 密钥。
            this.Koa2.keys = [config_1.config.initPlugins.SessionKey];
            this.PluginsList = [
                // 记录系统日志，访问级别的，记录用户的所有请求，作为koa的中间件
                logs_1.Logs.AccessLogger(),
                // 处理post请求
                koa_bodyparser_1.default(),
                // 静态资源配置
                koa_static_1.default(path_1.default.join(__dirname, config_1.config.initPlugins.staticPath), { maxage: 0 }),
                // session 配置
                koa_session_1.default({
                    key: 'koa:sess',
                    maxAge: 86400000,
                    overwrite: true,
                    httpOnly: true,
                    signed: true,
                    rolling: false,
                    renew: false,
                }, this.Koa2),
                // 模板引擎配置
                koa_views_1.default(path_1.default.join(__dirname, config_1.config.initPlugins.viewsPath), {
                    extension: 'jade',
                    options: { ext: 'jade' }
                })
            ];
            try {
                this.PluginsList.forEach((v) => this.Koa2.use(v));
                // @ts-ignore
                yield typeorm_1.createConnection(config_1.config.typeorm);
            }
            catch (e) {
                // 终端输出
                logger_1.logger.logSymbolsError(`配置插件出错：${e.message}`);
                logs_1.Logs.ApplicationLogger().error(e);
            }
        });
    }
}
exports.initPlugins = initPlugins;
