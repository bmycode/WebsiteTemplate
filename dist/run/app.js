"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const routing_controllers_1 = require("routing-controllers");
const typedi_1 = require("typedi");
const config_1 = require("../config");
const logs_1 = require("../utils/logs");
const init_1 = require("./init");
const logger_1 = require("../utils/logger");
const path_1 = __importDefault(require("path"));
class App extends init_1.initPlugins {
    constructor() {
        super();
        // typedi 注入到 routing-controllers
        routing_controllers_1.useContainer(typedi_1.Container);
        this.createKoa();
    }
    /**
     * 创建 koa 服务
     */
    createKoa() {
        this.app = routing_controllers_1.useKoaServer(this.Koa2, {
            cors: true,
            validation: true,
            classTransformer: true,
            controllers: [`${path_1.default.join(__dirname, '../controller/**/*{.js,.ts}')}`]
        });
        this.run();
    }
    // 启动程序
    run() {
        try {
            this.app.listen(config_1.config.port, () => logger_1.logger.logSymbolsSuccess(config_1.config.banner.welcome()));
        }
        catch (e) {
            logger_1.logger.logSymbolsError(`❌ 启动出错：${e.message}`);
            logs_1.Logs.ApplicationLogger().error(e);
        }
        this.errorCatch();
        // this.Page_404();
    }
    // 错误捕捉Unsupported type
    errorCatch() {
        this.app.on('error', err => {
            logger_1.logger.logSymbolsError(`❌ 主程序捕捉到错误：${err.message}`);
            logs_1.Logs.ApplicationLogger().error(err);
        });
    }
    /**
     * 捕捉到404的时候跳转到error错误页面
     * @constructor
     */
    Page_404() {
        this.app.use((ctx) => __awaiter(this, void 0, void 0, function* () {
            if (ctx.response.status == 404) {
                ctx.redirect('/error');
            }
        }));
    }
}
exports.App = App;
