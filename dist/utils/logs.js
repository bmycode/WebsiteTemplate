"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_log4_1 = __importDefault(require("koa-log4"));
const config_1 = require("../config");
koa_log4_1.default.configure(config_1.config.logConfig);
class Logs {
    /**
     * 访问级别的日志
     */
    static AccessLogger() {
        return koa_log4_1.default.koaLogger(koa_log4_1.default.getLogger('access'), { level: 'auto' });
    }
    /**
     * 应用级别的日志
     * @constructor
     */
    static ApplicationLogger() {
        return koa_log4_1.default.getLogger('application');
    }
    static HttpLogger() {
        return koa_log4_1.default.getLogger('request');
    }
}
exports.Logs = Logs;
