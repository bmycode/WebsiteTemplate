"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log_symbols_1 = __importDefault(require("log-symbols"));
const chalk_1 = __importDefault(require("chalk"));
class logger {
    static logSymbolsSuccess(message) {
        console.log(chalk_1.default.green.bold(log_symbols_1.default.success, message));
    }
    static logSymbolsInfo(message) {
        console.log(log_symbols_1.default.info, message);
    }
    static logSymbolsWarning(message) {
        console.log(log_symbols_1.default.warning, message);
    }
    static logSymbolsError(message) {
        console.log(chalk_1.default.bold.red(log_symbols_1.default.error, message));
    }
}
exports.logger = logger;
