"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../config/index");
class utils {
    /**
     * 统一设置网站浏览器的 title 标题内容
     * @param title
     */
    static titleName(title) {
        return `${title}${index_1.config.title}`;
    }
    /**
     * 将get地址栏参数转换为对象
     * @param url
     */
    static parseQueryString(url) {
        var reg_url = /^[^\?]+\?([\w\W]+)$/, reg_para = /([^&=]+)=([\w\W]*?)(&|$|#)/g, arr_url = reg_url.exec(url), ret = {};
        if (arr_url && arr_url[1]) {
            var str_para = arr_url[1], result;
            while ((result = reg_para.exec(str_para)) != null) {
                ret[result[1]] = result[2];
            }
        }
        return ret;
    }
}
exports.utils = utils;
