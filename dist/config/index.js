"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// 开发环境配置
const dev_1 = require("./dev");
// 上线环境配置
const prod_1 = require("./prod");
exports.config = process.env.NODE_ENV == "dev" ? dev_1.dev : prod_1.prod;
