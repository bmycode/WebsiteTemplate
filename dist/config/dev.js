"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
/**
 * 开发阶段环境配置
 */
exports.dev = {
    // 程序默认运行端口
    port: 80,
    // 绑定的域名
    host: "www.geekhelp.cn",
    // api接口基地址，如果改这个了，请改管理后台的axios基地址配置
    apiurl: '/bmy/api',
    // 初始化 koa2 插件需要的一些配置
    initPlugins: {
        SessionKey: '4WM7mSy5S5kkxcE1I3',
        staticPath: '../resources/static',
        viewsPath: '../resources/view' // 页面视图存放路径
    },
    // 终端的欢迎信息
    banner: {
        welcome: () => {
            return `
        👏  VR 产品介绍网站 👏

        👉  1：PC官网: http://${exports.dev.host}:${exports.dev.port}/
        👉  2：Admin后台: http://${exports.dev.host}:3030/#/course/all
        `;
        }
    },
    logConfig: {
        appenders: {
            //系统日志
            access: {
                type: 'dateFile',
                pattern: '_yyyy-MM-dd.log',
                alwaysIncludePattern: true,
                encoding: "utf-8",
                category: "access",
                filename: path_1.default.join(__dirname, '../../logs/access')
            },
            // 应用日志
            application: {
                type: 'dateFile',
                pattern: '_yyyy-MM-dd.log',
                alwaysIncludePattern: true,
                encoding: "utf-8",
                category: "application",
                filename: path_1.default.join(__dirname, '../../logs/application')
            },
            out: {
                type: 'console'
            }
        },
        categories: {
            default: { appenders: ['out'], level: 'info' },
            access: { appenders: ['access'], level: 'info' },
            application: { appenders: ['application'], level: 'WARN' }
        }
    },
    // 七牛的核心配置，不熟悉七牛请勿修改
    qiniu: {
        // 七牛秘钥，七牛云管理后台获得，可以改成自己的
        accessKey: '0MGmT_rkMiaOeXY09B4EhBnXuDcIYyKlGumQ-zUt',
        secretKey: '59dfmEi9Q50rMlz9sxTBCvYDhM7biERAjflBXALb',
        // 视频切片的配置
        HLSconfig: {
            // 被切片后视频存放的基路径，如果需要修改域名，请到域名管理后台 CNAME 泛解析域到七牛云
            // 然后再到七牛配置 泛子域名 将二级域名绑定到指定空间，这里 video.* 被绑定到 m3u8video 空间
            VideoBaseUrl: 'http://vsassets.geekhelp.cn/',
            // 需要被切片的源视频所在七牛云的空间名
            FromBucket: 'vrcource',
            // 视频被切片后存放的空间名
            ToBucket: 'm3u8video',
            // 视频切片规则，参考七牛云官网文档来配置，或者在 多媒体队列 中复制切片参数
            VideoHlsParams: 'avthumb/m3u8/segtime/10/ab/128k/ar/44100/acodec/libfaac/r/30/vb/1000k/vcodec/libx264/stripmeta/0/noDomain/1|saveas/'
        }
    },
    // 网站浏览器标签卡的基础标题
    title: ' | 怪客课堂',
    // 本地Mysql数据库配置
    typeorm: {
        type: 'mysql',
        host: "localhost",
        charset: "utf8_general_ci",
        port: 3306,
        username: "root",
        password: "lb714500",
        database: "shop",
        logging: "all",
        synchronize: false,
        logger: "advanced-console",
        cache: {
            duration: 30000
        },
        maxQueryExecutionTime: 1000,
        entities: [
            `${path_1.default.join(__dirname, '../entity/*{.js,.ts}')}`
        ]
    }
};
