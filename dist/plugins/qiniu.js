"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const qiniu_1 = __importDefault(require("qiniu"));
const typedi_1 = require("typedi");
const index_1 = require("../config/index");
let QiNiu = class QiNiu {
    constructor() {
        this.FromBucket = index_1.config.qiniu.HLSconfig.FromBucket;
        this.ToBucket = index_1.config.qiniu.HLSconfig.ToBucket;
        this.VideoHlsParams = index_1.config.qiniu.HLSconfig.VideoHlsParams;
        this.accessKey = index_1.config.qiniu.accessKey;
        this.secretKey = index_1.config.qiniu.secretKey;
        this.mac = new qiniu_1.default.auth.digest.Mac(this.accessKey, this.secretKey);
    }
    /**
     *
     * @param BucketName
     * @constructor
     */
    GenerateToken(BucketName = "vrcource") {
        let putPolicy = new qiniu_1.default.rs.PutPolicy({
            scope: BucketName,
            expires: 7200
        });
        return putPolicy.uploadToken(this.mac);
    }
    /**
     * 根据前端传递的 filename 文件名，对需要的视频进行切片配置，
     * 返回给前端 token，前端拿到 token 后通过form data将视频和
     * token 传递给七牛，七牛会根据token开始切片任务。这里不需要
     * 配置七牛切片成功后的回调通知，因为已经可以通过基地址+m3u8文件名拿到
     * 切片后的地址
     * @param params
     * @constructor
     */
    GenerateHlsToken(params) {
        let M3u8FileName = new Date().getTime();
        let saveHlsEntry = qiniu_1.default.util.urlsafeBase64Encode(`${this.ToBucket}:${M3u8FileName}`);
        const options = {
            scope: `${this.FromBucket}:${params['filename']}`,
            persistentOps: `${this.VideoHlsParams}${saveHlsEntry}`,
            persistentPipeline: 'guaikem3u8',
        };
        let putPolicy = new qiniu_1.default.rs.PutPolicy(options);
        return {
            filePath: `${index_1.config.qiniu.HLSconfig.VideoBaseUrl}${M3u8FileName}`,
            token: putPolicy.uploadToken(this.mac)
        };
    }
};
QiNiu = __decorate([
    typedi_1.Service(),
    __metadata("design:paramtypes", [])
], QiNiu);
exports.QiNiu = QiNiu;
