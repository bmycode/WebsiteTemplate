"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const ResultDataMiddlewares_1 = require("./ResultDataMiddlewares");
const typedi_1 = require("typedi");
const logs_1 = require("../utils/logs");
// 拦截controller在数据响应给前端，按照标准格式输出数据，同时session存储用户
let JsonResponseInterceptor = class JsonResponseInterceptor {
    intercept(action, content) {
        if (content == undefined || content.length == 0) {
            logs_1.Logs.ApplicationLogger().error(action);
            return this.resultDataMiddlewares.error(action, "没有数据");
        }
        else if (content.length != 0) {
            return this.resultDataMiddlewares.success(content);
        }
    }
};
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", ResultDataMiddlewares_1.ResultDataMiddlewares)
], JsonResponseInterceptor.prototype, "resultDataMiddlewares", void 0);
JsonResponseInterceptor = __decorate([
    typedi_1.Service()
], JsonResponseInterceptor);
exports.JsonResponseInterceptor = JsonResponseInterceptor;
