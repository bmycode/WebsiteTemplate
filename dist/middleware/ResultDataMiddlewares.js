"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
let ResultDataMiddlewares = class ResultDataMiddlewares {
    get status() {
        return this._status;
    }
    set status(value) {
        this._status = value;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    get methods() {
        return this._methods;
    }
    set methods(value) {
        this._methods = value;
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
    /**
     * 数据请求成功
     * @param _data 需要返回给前端的数据
     */
    success(_data) {
        this.status = 200;
        this.data = _data;
        return JSON.stringify({
            status: this.status,
            time: new Date(),
            result: this.data
        });
    }
    /**
     * 数据请求失败
     * @param action 请求对象
     */
    error(action, message) {
        this.status = action.response.status; // 状态码
        this.data = message; // 错误信息
        this.methods = action.request.method; // 请求方式
        this.url = action.request.url; // 请求地址
        return JSON.stringify({
            methods: this.methods,
            status: this.status,
            result: this.data,
            url: this.url
        });
    }
};
ResultDataMiddlewares = __decorate([
    typedi_1.Service()
], ResultDataMiddlewares);
exports.ResultDataMiddlewares = ResultDataMiddlewares;
