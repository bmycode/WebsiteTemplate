"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 判断用户是否登录，如果登录则继续访问，没有去登录
 */
class CheckLoginMiddleware {
    use(context, next) {
        if (!context.session || !context.session.user) {
            context.redirect(`/reg?from=${context.request.url}`);
        }
        else {
            return next();
        }
    }
}
exports.CheckLoginMiddleware = CheckLoginMiddleware;
