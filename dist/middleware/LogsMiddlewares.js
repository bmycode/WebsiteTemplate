"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const logs_1 = require("../utils/logs");
const moment_1 = __importDefault(require("moment"));
/**
 * controller 拦截器
 * 拦截 controller 被访问前，进行访问的日志记录
 */
let LogsMiddlewares = class LogsMiddlewares {
    use(context, next) {
        if (context.request.method == "GET") {
            logs_1.Logs.ApplicationLogger().warn(`${moment_1.default().format('YYYY-MM-DD HH:mm:ss')} ${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.querystring)} ${JSON.stringify(context.request.header)} \n`);
        }
        else {
            logs_1.Logs.ApplicationLogger().warn(`${moment_1.default().format('YYYY-MM-DD HH:mm:ss')} ${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.body)} ${JSON.stringify(context.request.header)} \n`);
        }
        return next();
    }
};
LogsMiddlewares = __decorate([
    typedi_1.Service()
], LogsMiddlewares);
exports.LogsMiddlewares = LogsMiddlewares;
