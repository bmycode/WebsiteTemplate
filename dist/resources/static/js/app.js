requirejs.config({
  baseUrl: '/js',
  waitSeconds: 0,
  paths: {
    app: 'app',
    jquery: 'lib/jquery.min',
    swiper: 'lib/swiper/swiper.min',
    zui: 'lib/zui/js/zui.min',
    utils: 'utils/index',
    config: 'config/index'
  },
  map: {
    '*': {
      'css': 'lib/css'
    }
  },
  shim : { // 按需加载css
    'app/index': { deps: ['css!/css/home/index.css','css!/js/lib/swiper/swiper.min.css'] },
    'app/about': { deps: ['css!/css/home/about.css'] },
    'app/register': { deps: ['css!/css/home/register.css'] }
  }
});

// 读取每个页面自定义属性data-module获取js文件名，然后自动加载并调用非对应页面的js文件中的 init 方法
requirejs(["jquery"],function ($) {
  var module = $('.pages').attr('data-module');
  if(module != undefined){
    requirejs([ `app/${ module }` ], function (module) {
      module.init();
    })
  }
});